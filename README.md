Voici le programme que vous m'aviez demandé pour évaluer mes capacités de code sur c# en utilisant Unity. 

J'ai développé un Pong en suivant un tutoriel dans un premier temps pour me familiariser avec le programme. **La majorité du code du Pong n'est donc pas de moi**,
mais le système de point et l'accélération de la balle ne faisaient pas partie du tutoriel. J'ai trouvé intéressant de l'ajouter au projet pour expérimenter avec 
le développement de menus. Le jeu renvoie les joueurs au menu principal lorsque l'un des deux atteint 10 points. 

Le tetris ,lui, a été développé en utilisant plusieurs petits tutoriels sans aucune relation au jeu. J'ai repris une architecture similaire à celle que j'utilisais
en java, où mon script "mouvement" a un rôle similaire à l'interface et mon script "tiles" à celui du domaine. Je compte possiblement l'optimiser pour mieux tirer profit
des différentes fonctions de Unity que je ne connais pas encore. 
Lorsque le joueur rempli la grille, il est renvoyé au menu principal. Le reste du "gameplay" est identique à un Tetris régulier. 

Je reste disponible pour d'éventuelle questions, et j'espère ravoir de vos nouvelles,

Sophie Bourbonnais-Roy